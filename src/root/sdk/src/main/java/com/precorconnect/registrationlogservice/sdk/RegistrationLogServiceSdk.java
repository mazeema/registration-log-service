package com.precorconnect.registrationlogservice.sdk;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;


public interface RegistrationLogServiceSdk {
	
	Long addRegistrationLog(
			@NonNull AddRegistrationLog request,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException;
	
	void updateRegistrationLog(
			@NonNull List<UpdateRegistrationLog> request,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException;
	
	void updateEWStatusForRegistrationLog(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull  OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException;

}
