package com.precorconnect.registrationlogservice.sdk;

import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;

public class RegistrationLogServiceSdkImpl implements RegistrationLogServiceSdk {
	
	/*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public RegistrationLogServiceSdkImpl(
            @NonNull RegistrationLogServiceSdkConfig config
    ) {
        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );
        injector =
                Guice.createInjector(guiceModule);
    }

	@Override
	public Long addRegistrationLog(
			@NonNull AddRegistrationLog request,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {

		return injector
				.getInstance(AddRegistrationLogFeature.class)
				.execute(request, accessToken);
	}

	@Override
	public void updateRegistrationLog(
			@NonNull List<UpdateRegistrationLog> request,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException {
		
		injector
				.getInstance(UpdateRegistrationLogFeature.class)
				.execute(request, accessToken);
	}

	@Override
	public void updateEWStatusForRegistrationLog(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId, 
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {
		
		injector
				.getInstance(UpdateEWStatusForRegistrationLogFeature.class)
				.execute(extendedWarrantyStatus, partnerSaleRegistrationId, accessToken);
		
	}
		
	

}
