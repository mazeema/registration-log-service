package com.precorconnect.registrationlogservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;

public class UpdateEWStatusForRegistrationLogFeatureImpl implements UpdateEWStatusForRegistrationLogFeature {
	
private final WebTarget baseWebTarget;
	
	@Inject
	public UpdateEWStatusForRegistrationLogFeatureImpl(
			@NonNull WebTarget baseWebTarget
			) {
		
		this.baseWebTarget = guardThat(
                "baseWebTarget",
                baseWebTarget
				)
                .isNotNull()
                .thenGetValue()
                .path("registration-log/updateewstatusforregistrationlog");
	}

	@Override
	public void execute(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException {

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        accessToken.getValue()
                );
		
		    
		 baseWebTarget
		    					.queryParam("draftId", partnerSaleRegistrationId.getValue())
		    					.queryParam("ewstatus", extendedWarrantyStatus.getValue())
		    					.request(MediaType.APPLICATION_JSON_TYPE)
		    					.header("Authorization", authorizationHeaderValue)
		    					.post(null);

		

	}

}
