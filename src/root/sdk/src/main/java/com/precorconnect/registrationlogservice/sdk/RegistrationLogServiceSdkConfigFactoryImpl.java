package com.precorconnect.registrationlogservice.sdk;

import java.net.MalformedURLException;
import java.net.URL;

public class RegistrationLogServiceSdkConfigFactoryImpl 
				implements RegistrationLogServiceSdkConfigFactory {

	@Override
	public RegistrationLogServiceSdkConfig construct() {

		return 
				new RegistrationLogServiceSdkConfigImpl(
						constructprecorConnectApiBaseUrl()
				);
	}

	public URL constructprecorConnectApiBaseUrl(){

		try {

            String precorConnectApiBaseUrl=System
            		            .getenv("PRECOR_CONNECT_API_BASE_URL");

            return
            		new URL(
            		  precorConnectApiBaseUrl
            		);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }
	}

}
