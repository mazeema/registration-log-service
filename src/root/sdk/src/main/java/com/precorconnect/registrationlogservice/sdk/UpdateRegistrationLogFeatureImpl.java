package com.precorconnect.registrationlogservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;

public class UpdateRegistrationLogFeatureImpl 
						implements UpdateRegistrationLogFeature {
	
	private final WebTarget baseWebTarget;
	
	@Inject
	public UpdateRegistrationLogFeatureImpl(
			@NonNull WebTarget baseWebTarget) {
		
		this.baseWebTarget = guardThat(
                "baseWebTarget",
                baseWebTarget
				)
                .isNotNull()
                .thenGetValue()
                .path("registration-log/updateRegistrationLog");
	}

	@Override
	public void execute(
			@NonNull List<UpdateRegistrationLog> request,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        accessToken.getValue()
                );
		
		 baseWebTarget
			.request(MediaType.APPLICATION_JSON_TYPE)
			.header("Authorization", authorizationHeaderValue)
			.put(Entity.entity(
                                		request,
                                        MediaType.APPLICATION_JSON
                                )
			);
	}

}
