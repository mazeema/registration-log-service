package com.precorconnect.registrationlogservice.sdk;

import java.net.URI;
import java.net.URISyntaxException;

public class Dummy {

    /*
    fields
     */
    private final Long partnerSaleRegistrationId = 100L;

    private final String partnerAccountId = "001K000001H2Km2IAF";
    
    private final String accountName ="test";
    
    private final String sellDate = "05/11/2016";

	private final String installDate = "05/11/2016";
	
	private final String submittedDate = "05/11/2016";
	
	private final String spiffStatus = "Not Submitted";

    private final URI uri;
        
    

    /*
    constructors
     */
    public Dummy() {
        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
    }



	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}



	public String getPartnerAccountId() {
		return partnerAccountId;
	}



	public String getAccountName() {
		return accountName;
	}



	public String getSellDate() {
		return sellDate;
	}



	public String getInstallDate() {
		return installDate;
	}



	public String getSubmittedDate() {
		return submittedDate;
	}



	public URI getUri() {
		return uri;
	}



	public String getSpiffStatus() {
		return spiffStatus;
	}    
    
}
