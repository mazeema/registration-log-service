package com.precorconnect.registrationlogservice.sdk;

import com.precorconnect.identityservice.HmacKey;
import com.precorconnect.identityservice.HmacKeyImpl;

public class ConfigFactory {

    Config construct() {

        return new Config(               
                constructIdentityServiceJwtSigningKey()
        );

    }

    private HmacKey constructIdentityServiceJwtSigningKey() {

        String identityServiceJwtSigningKey =
                System.getenv("TEST_IDENTITY_SERVICE_JWT_SIGNING_KEY");

        return
                new HmacKeyImpl(
                        identityServiceJwtSigningKey
                );

    }

   

}
