package com.precorconnect.registrationlogservice.sdk;


import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatusImpl;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;
import com.precorconnect.registrationlogservice.webapi.Application;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class RegistrationLogServiceSdkIT {

    /*
    fields
     */
    @Value("${local.server.port}")
    int port;

    private final Dummy dummy = new Dummy();

    private final Config config =
            new ConfigFactory()
                    .construct();

    private final Factory factory =
            new Factory(
                    dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                            config.getIdentityServiceJwtSigningKey()
                    )
            );

    /*
    test methods
     */
    @Test
    public void addRegistrationLog_ReturnsRegistrationId(
    ) throws Exception {

        /*
        arrange
         */
        RegistrationLogServiceSdk objectUnderTest =
                new RegistrationLogServiceSdkImpl(
                        factory.constructRegistrationLogServiceSdkConfig(port)
                );

        // add a registration log r
        AddRegistrationLog addRegistrationLog =
                new AddRegistrationLog(
                        dummy.getPartnerSaleRegistrationId(),
                        dummy.getPartnerAccountId(),
                        dummy.getAccountName(),
                        dummy.getSellDate(),
                        dummy.getInstallDate(),
                        dummy.getSubmittedDate()
                );
        /*
        act
         */
        Long registrationId = objectUnderTest.addRegistrationLog(
        		addRegistrationLog,
                factory.constructValidPartnerRepOAuth2AccessToken()
        );

        /*
        assert
         */
        assertThat(registrationId).isNotNull();

    }

    @Test
    public void updateRegistrationLog_ReturnsNothig(
    ) throws Exception {

        /*
        arrange
         */
    	RegistrationLogServiceSdk objectUnderTest =
                new RegistrationLogServiceSdkImpl(
                        factory.constructRegistrationLogServiceSdkConfig(port)
                );

    	List<UpdateRegistrationLog> updateRegistrationLogs = new ArrayList<UpdateRegistrationLog>();
        UpdateRegistrationLog updateRegistrationLog =
                new UpdateRegistrationLog(
                        dummy.getPartnerSaleRegistrationId(),
                        dummy.getSpiffStatus()                        
                );
        updateRegistrationLogs.add(updateRegistrationLog);
        
                objectUnderTest.updateRegistrationLog(
                		updateRegistrationLogs,
                        factory.constructValidPartnerRepOAuth2AccessToken()
                );
    }
    
    @Test
    public void updateEWStatusForRegistrationLog_ReturnsNothing(
    ) throws Exception {

        /*
        arrange
         */
    	RegistrationLogServiceSdk objectUnderTest =
                new RegistrationLogServiceSdkImpl(
                        factory.constructRegistrationLogServiceSdkConfig(port)
                );

    	objectUnderTest
    				.updateEWStatusForRegistrationLog(
    						new ExtendedWarrantyStatusImpl("submitted"), 
    						new PartnerSaleRegistrationIdImpl(
    								740l
    							), 
    						factory.constructValidPartnerRepOAuth2AccessToken()
    						);
    	
        
        
        
    }

}
