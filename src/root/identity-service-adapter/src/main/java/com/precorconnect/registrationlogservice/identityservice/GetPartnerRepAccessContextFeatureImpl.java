package com.precorconnect.registrationlogservice.identityservice;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.AuthenticationException;
import com.precorconnect.PartnerRepAccessContext;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class GetPartnerRepAccessContextFeatureImpl
        implements GetPartnerRepAccessContextFeature {

    /*
    fields
     */
    private final IdentityServiceSdk identityServiceSdk;

    /*
    constructors
     */
    @Inject
    public GetPartnerRepAccessContextFeatureImpl(
            @NonNull IdentityServiceSdk identityServiceSdk
    ) {

    	this.identityServiceSdk =
                guardThat(
                        "identityServiceSdk",
                        identityServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public PartnerRepAccessContext execute(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return identityServiceSdk
                .getPartnerRepAccessContext(
                        accessToken
                );

    }
}
