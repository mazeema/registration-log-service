package com.precorconnect.registrationlogservice.identityservice;

public interface IdentityServiceAdapterConfigFactory {

	IdentityServiceAdapterConfig construct();

}
