package com.precorconnect.registrationlogservice.identityservice;

import java.net.URL;

public interface IdentityServiceAdapterConfig {

    URL getPrecorConnectApiBaseUrl();

}
