package com.precorconnect.registrationlogservice.api;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.RegistrationLogView;
import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLog;



public interface RegistrationLogService {
	
	PartnerSaleRegistrationId addRegistrationLog(
				@NonNull AddPartnerSaleRegistrationLog request,
				@NonNull  OAuth2AccessToken accessToken
				) throws AuthenticationException, AuthorizationException;


	Collection<RegistrationLogView> listRegistrationLogWithId(
				@NonNull AccountId accountId,
				@NonNull OAuth2AccessToken accessToken
				) throws AuthenticationException, AuthorizationException;
	 
	 
	void updateRegistrationLog(
				@NonNull List<UpdatePartnerSaleRegistrationLog> request,
				@NonNull  OAuth2AccessToken accessToken
				) throws AuthenticationException, AuthorizationException;
	
	void updateEWStatusForRegistrationLog(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull  OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException;

}
