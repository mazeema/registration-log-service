package com.precorconnect.registrationlogservice.api;

import com.precorconnect.registrationlogservice.database.DatabaseAdapterConfig;
import com.precorconnect.registrationlogservice.identityservice.IdentityServiceAdapterConfig;


public interface RegistrationLogServiceConfig {

    DatabaseAdapterConfig getDatabaseAdapterConfig();
    
    IdentityServiceAdapterConfig getIdentityServiceAdapterConfig();

}
