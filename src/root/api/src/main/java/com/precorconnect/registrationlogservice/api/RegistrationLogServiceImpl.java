package com.precorconnect.registrationlogservice.api;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.Core;
import com.precorconnect.registrationlogservice.CoreImpl;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.RegistrationLogView;
import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.database.DatabaseAdapterImpl;
import com.precorconnect.registrationlogservice.identityservice.IdentityServiceAdapterImpl;


public class RegistrationLogServiceImpl
		implements RegistrationLogService {

    /*
    fields
     */
    private final Core core;

    /*
    constructors
     */
    @Inject
    public RegistrationLogServiceImpl(
            @NonNull RegistrationLogServiceConfig config
    ) {

        core =
                new CoreImpl(
                        new DatabaseAdapterImpl(
                                config.getDatabaseAdapterConfig()
                        ),
                        new IdentityServiceAdapterImpl(
                                config.getIdentityServiceAdapterConfig()
                        )
                );
        		  		
    }

	

	@Override
	public Collection<RegistrationLogView> listRegistrationLogWithId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {

		return
				core
					.listRegistrationLogWithId(
							accountId,
							accessToken
							);
	}


	@Override
	public PartnerSaleRegistrationId addRegistrationLog(
			@NonNull AddPartnerSaleRegistrationLog request,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {
			
		
		return
				core
					.addRegistrationLog(
							request,
							accessToken
							);
	}



	@Override
	public void updateRegistrationLog(
			@NonNull List<UpdatePartnerSaleRegistrationLog> request,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {
		
		core
			.updateRegistrationLog(
				request,
				accessToken
				);
		
	}



	@Override
	public void updateEWStatusForRegistrationLog(@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId, @NonNull OAuth2AccessToken accessToken)
					throws AuthenticationException, AuthorizationException {
		
		core
		     .updateEWStatusForRegistrationLog(
		    		 extendedWarrantyStatus, 
		    		 partnerSaleRegistrationId,
		    		 accessToken
		    );
		
	}



	

	
}
