package com.precorconnect.registrationlogservice;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.AccountName;
import com.precorconnect.AccountNameImpl;
import com.precorconnect.OAuth2AccessToken;

public class Dummy {

    /*
    fields
     */
    private URI uri;

    private AccountId accountId = new AccountIdImpl("001K000001H2Km2IAF");
       
    private final PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(1L);
    
    private final AccountName accountName =new AccountNameImpl("CucumberTest");
    
    private final SellDate sellDate = new SellDateImpl(Instant.now());

	private final InstallDate installDate = new InstallDateImpl(Instant.now());
	
	private final SubmittedDate submittedDate = new SubmittedDateImpl(Instant.now());
	
	private final ExtendedWarrantyStatus EWStatus = new ExtendedWarrantyStatusImpl("Not Submitted");
	
	private final SpiffStatus spiffStatus = new SpiffStatusImpl("Not Submitted");
    /*
    constructors
     */
    public Dummy() {

        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
        
    }

    /*
    getter methods
     */
    public URI getUri() {
        return uri;
    }

	public AccountId getAccountId() {
		return accountId;
	}

	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public AccountName getAccountName() {
		return accountName;
	}

	public SellDate getSellDate() {
		return sellDate;
	}

	public InstallDate getInstallDate() {
		return installDate;
	}

	public SubmittedDate getSubmittedDate() {
		return submittedDate;
	}

	public ExtendedWarrantyStatus getEWStatus() {
		return EWStatus;
	}

	public SpiffStatus getSpiffStatus() {
		return spiffStatus;
	}

	

}
