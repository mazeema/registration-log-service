package com.precorconnect.registrationlogservice;

import java.util.Optional;

import com.precorconnect.AccountId;
import com.precorconnect.AccountName;

public interface AddPartnerSaleRegistrationLog {
	
	PartnerSaleRegistrationId getPartnerSaleRegistrationId();
	
	AccountId getAccountId();
	
	AccountName getAccountName();
	
	Optional<InstallDate> getInstallDate();

    SellDate getSellDate();
    
    SubmittedDate getSubmittedDate();
    
    ExtendedWarrantyStatus getExtendedWarrantyStatus();
    
    SpiffStatus getSpiffStatus();

}
