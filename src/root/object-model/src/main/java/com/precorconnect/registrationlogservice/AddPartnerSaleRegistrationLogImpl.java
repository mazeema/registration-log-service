package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.AccountName;

public class AddPartnerSaleRegistrationLogImpl 
				implements AddPartnerSaleRegistrationLog {
	
	private PartnerSaleRegistrationId partnerSaleRegistrationId;
	private AccountId accountId;
	private AccountName accountName;
	private SellDate sellDate;
	private InstallDate installDate;
	private SubmittedDate submittedDate;
	private ExtendedWarrantyStatus extendedWarrantyStatus;
	private SpiffStatus spiffStatus;
	
	 public AddPartnerSaleRegistrationLogImpl(
	            @NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
	            @NonNull AccountId accountId,
	            @NonNull AccountName accountName,
	            @NonNull SellDate sellDate,
	            @Nullable InstallDate installDate,
	            @NonNull SubmittedDate submittedDate,
	            @NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
	            @NonNull SpiffStatus spiffStatus
	    ) {

	        this.partnerSaleRegistrationId =
	                guardThat(
	                        "partnerSaleRegistrationId",
	                        partnerSaleRegistrationId
	                )
	                        .isNotNull()
	                        .thenGetValue();

	        this.accountId =
	                guardThat(
	                        "accountId",
	                        accountId
	                )
	                        .isNotNull()
	                        .thenGetValue();
	        
	        this.accountName =
	                guardThat(
	                        "accountName",
	                        accountName
	                )
	                        .isNotNull()
	                        .thenGetValue();

	        this.sellDate =
	        		guardThat(
	        				"sellDate",
	        				sellDate
	        				)
	        				.isNotNull()
	        				.thenGetValue();
	        
	        this.installDate = installDate;
	               
	        this.submittedDate =
	                guardThat(
	                        "submittedDate",
	                        submittedDate
	                )
	                        .isNotNull()
	                        .thenGetValue();
	        
	        this.extendedWarrantyStatus =
	                guardThat(
	                        "extendedWarrantyStatus",
	                        extendedWarrantyStatus
	                )
	                        .isNotNull()
	                        .thenGetValue();
	        
	        this.spiffStatus =
	                guardThat(
	                        "spiffStatus",
	                        spiffStatus
	                )
	                        .isNotNull()
	                        .thenGetValue();

	    }


	@Override
	public PartnerSaleRegistrationId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public AccountId getAccountId() {
		return accountId;
	}

	@Override
	public AccountName getAccountName() {
		return accountName;
	}

	@Override
	public Optional<InstallDate> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	@Override
	public SellDate getSellDate() {
		return sellDate;
	}

	@Override
	public SubmittedDate getSubmittedDate() {
		return submittedDate;
	}

	@Override
	public ExtendedWarrantyStatus getExtendedWarrantyStatus() {
		return extendedWarrantyStatus;
	}


	@Override
	public SpiffStatus getSpiffStatus() {
		return spiffStatus;
	}

}
