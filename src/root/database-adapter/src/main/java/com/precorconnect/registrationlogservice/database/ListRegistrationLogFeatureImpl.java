package com.precorconnect.registrationlogservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.AccountId;
import com.precorconnect.registrationlogservice.RegistrationLogView;

@Singleton
public class ListRegistrationLogFeatureImpl 
				implements ListRegistrationLogFeature{

	private final SessionFactory sessionFactory;

    private final ListRegistrationLogViewFactory listRegistrationLogViewFactory;

    @Inject
    public ListRegistrationLogFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final ListRegistrationLogViewFactory listRegistrationLogViewFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.listRegistrationLogViewFactory =
                guardThat(
                        "listRegistrationLogViewFactory",
                        listRegistrationLogViewFactory
                )
                        .isNotNull()
                        .thenGetValue();


    }

	@SuppressWarnings("unchecked")
	@Override
	public Collection<RegistrationLogView> listRegistrationLogWithId(
			@NonNull AccountId accountId
			) {

		Session  session = sessionFactory.openSession();
        try {

        	Query query = session.createQuery("from RegistrationLog where partnerAccountId = :id");

        	query.setParameter("id", accountId.getValue());

        	List<RegistrationLog> registrationLogs = query.list();
        	
            return registrationLogs
                    .stream()
                    .map(listRegistrationLogViewFactory::construct)
                    .collect(Collectors.toList());

        } catch(final Exception e) {

        	throw new RuntimeException("Exception while retriving Registration Logs:", e);

        } finally {

            if (session != null) {
                session.close();
            }

        }

	}

}
