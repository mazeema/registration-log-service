package com.precorconnect.registrationlogservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

class GuiceModule
        extends AbstractModule {

    /*
    fields
     */
    private final DatabaseAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull final DatabaseAdapterConfig config
    ) {

        this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindFactories();

        bindFeatures();

    }

    private void bindFactories() {

    	bind(PartnerRegistrationLogFactory.class)
 				.to(PartnerRegistrationLogFactoryImpl.class);

        bind(ListRegistrationLogViewFactory.class)
        		.to(ListSaleRegLogViewFactoryImpl.class);

    }

    private void bindFeatures() {
    	
    	bind(AddRegistrationLogFeature.class)
				.to(AddRegistrationLogFeatureImpl.class);

        bind(ListRegistrationLogFeature.class)
				.to(ListRegistrationLogFeatureImpl.class);
        
        bind(UpdateRegistrationLogFeature.class)
			.to(UpdateRegistrationLogFeatureImpl.class);
        
        bind(UpdateEWStatusForRegistrationLogFeature.class)
		.to(UpdateEWStatusForRegistrationLogFeatureImpl.class);


    }

    @Provides
    @Singleton
    SessionFactory sessionFactory() {

        Configuration configuration = new Configuration();
        configuration.addPackage(getClass().getPackage().getName());
        configuration.addAnnotatedClass(RegistrationLog.class);
        configuration.configure();

        configuration.setProperty("hibernate.connection.url", config.getUri().toString());
        configuration.setProperty("hibernate.connection.username", config.getUsername().getValue());
        configuration.setProperty("hibernate.connection.password", config.getPassword().getValue());

        StandardServiceRegistryBuilder builder =
                new StandardServiceRegistryBuilder()
                        .applySettings(
                                configuration.getProperties());

        return configuration
                .buildSessionFactory(
                        builder.build());
    }
}
