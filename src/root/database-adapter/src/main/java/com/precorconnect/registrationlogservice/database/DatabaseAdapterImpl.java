package com.precorconnect.registrationlogservice.database;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.flywaydb.core.Flyway;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.DatabaseAdapter;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.RegistrationLogView;
import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLog;

public class DatabaseAdapterImpl implements
        DatabaseAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public DatabaseAdapterImpl(
            @NonNull DatabaseAdapterConfig config
    ) {

        // ensure database schema up to date
        Flyway flyway = new Flyway();
        flyway.setDataSource(
                config.getUri().toString(),
                config.getUsername().getValue(),
                config.getPassword().getValue());
        flyway.migrate();


        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);

    }
    
    @Override
	public PartnerSaleRegistrationId addRegistrationLog(
			@NonNull AddPartnerSaleRegistrationLog request) {
		
		return 
		
				injector
         		.getInstance(AddRegistrationLogFeature.class)
         		.execute(request);
		
	}

   

	@Override
	public Collection<RegistrationLogView> listSpiffLogsWithId(
			@NonNull AccountId accountId) {
		return
				injector
					.getInstance(ListRegistrationLogFeature.class)
					.listRegistrationLogWithId(accountId);
	}

	@Override
	public void updateRegistrationLog(
			@NonNull List<UpdatePartnerSaleRegistrationLog> request) {
		
		injector
				.getInstance(UpdateRegistrationLogFeature.class)
				.execute(request);
		
	}

	@Override
	public void updateEWStatusForRegistrationLog(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId
			) {
		
		injector
				.getInstance(UpdateEWStatusForRegistrationLogFeature.class)
				.execute(extendedWarrantyStatus, partnerSaleRegistrationId);
		
	}

}
