package com.precorconnect.registrationlogservice.database;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLog;

public interface UpdateRegistrationLogFeature {
	
	void execute(
            @NonNull List<UpdatePartnerSaleRegistrationLog> request
    );

}
