package com.precorconnect.registrationlogservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.registrationlogservice.RegistrationLogView;

public interface ListRegistrationLogFeature {
	
	Collection<RegistrationLogView> 
						listRegistrationLogWithId(
							@NonNull AccountId accountId
							);
}
