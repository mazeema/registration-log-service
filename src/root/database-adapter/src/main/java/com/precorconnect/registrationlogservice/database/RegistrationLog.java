package com.precorconnect.registrationlogservice.database;



import java.util.Date;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RegistrationLog")
class RegistrationLog {

    /*
    fields
     */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private Long partnerSaleRegistrationId;
    
    private String partnerAccountId;

    private Date sellDate;

    private Date installDate;

    private Date submittedDate;

    private String facilityName;

    private String extendedWarrantyStatus;

    private String spiffStatus;
    
    
	public Long getId() {
		return id;
	}

	public String getPartnerAccountId() {
		return partnerAccountId;
	}

	public void setPartnerAccountId(String partnerAccountId) {
		this.partnerAccountId = partnerAccountId;
	}

	public void setId(Long Id) {
		this.id = Id;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public void setPartnerSaleRegistrationId(Long partnerSaleRegistrationId) {
		this.partnerSaleRegistrationId = partnerSaleRegistrationId;
	}

	public Date getSellDate() {
		return sellDate;
	}

	public void setSellDate(Date sellDate) {
		this.sellDate = sellDate;
	}

	public Optional<Date> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	public void setInstallDate(Date installDate) {
		this.installDate = installDate;
	}

	public Date getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Date submittedDate) {
		this.submittedDate = submittedDate;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getExtendedWarrantyStatus() {
		return extendedWarrantyStatus;
	}

	public void setExtendedWarrantyStatus(String extendedWarrantyStatus) {
		this.extendedWarrantyStatus = extendedWarrantyStatus;
	}

	public String getSpiffStatus() {
		return spiffStatus;
	}

	public void setSpiffStatus(String spiffStatus) {
		this.spiffStatus = spiffStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result 
				+ ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((partnerSaleRegistrationId == null) ? 0 : partnerSaleRegistrationId.hashCode());
		result = prime * result
				+ ((sellDate == null) ? 0 : sellDate.hashCode());
		result = prime * result
				+ ((installDate == null) ? 0 : installDate.hashCode());
		result = prime * result
				+ ((installDate == null) ? 0 : installDate.hashCode());
		result = prime * result
				+ ((submittedDate == null) ? 0 : submittedDate.hashCode());
		result = prime * result
				+ ((facilityName == null) ? 0 : facilityName.hashCode());
		result = prime * result
				+ ((extendedWarrantyStatus == null) ? 0 : extendedWarrantyStatus.hashCode());
		result = prime * result
				+ ((spiffStatus == null) ? 0 : spiffStatus.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RegistrationLog other = (RegistrationLog) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (partnerSaleRegistrationId == null) {
			if (other.partnerSaleRegistrationId != null) {
				return false;
			}
		} else if (!partnerSaleRegistrationId.equals(other.partnerSaleRegistrationId)) {
			return false;
		}
		if (installDate == null) {
			if (other.installDate != null) {
				return false;
			}
		} else if (!installDate.equals(other.installDate)) {
			return false;
		}
		if (sellDate == null) {
			if (other.sellDate!= null) {
				return false;
			}
		} else if (!sellDate.equals(other.sellDate)) {
			return false;
		}
		if (submittedDate == null) {
			if (other.submittedDate != null) {
				return false;
			}
		} else if (!submittedDate.equals(other.submittedDate)) {
			return false;
		}
		if (facilityName == null) {
			if (other.facilityName != null) {
				return false;
			}
		} else if (!facilityName.equals(other.facilityName)) {
			return false;
		}
		if (extendedWarrantyStatus == null) {
			if (other.extendedWarrantyStatus != null) {
				return false;
			}
		} else if (!extendedWarrantyStatus
				.equals(other.extendedWarrantyStatus)) {
			return false;
		}
		if (spiffStatus == null) {
			if (other.spiffStatus != null) {
				return false;
			}
		} else if (!spiffStatus.equals(other.spiffStatus)) {
			return false;
		}
		return true;
	}

}
