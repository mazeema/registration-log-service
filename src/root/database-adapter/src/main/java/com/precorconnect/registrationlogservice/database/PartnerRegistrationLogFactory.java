package com.precorconnect.registrationlogservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;

public interface PartnerRegistrationLogFactory {
	
	RegistrationLog  construct(
            			@NonNull AddPartnerSaleRegistrationLog addPartnerSaleRegistrationLog
						);

}
