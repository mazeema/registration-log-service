package com.precorconnect.registrationlogservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;

@Singleton
public class AddRegistrationLogFeatureImpl 
					implements AddRegistrationLogFeature {
	
	 /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final PartnerRegistrationLogFactory partnerRegistrationLogFactory;

    /*
    constructors
     */
    @Inject
    public AddRegistrationLogFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull PartnerRegistrationLogFactory partnerRegistrationLogFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

        this.partnerRegistrationLogFactory =
                guardThat(
                        "partnerRegistrationLogFactory",
                        partnerRegistrationLogFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public PartnerSaleRegistrationId execute(
			@NonNull AddPartnerSaleRegistrationLog request
			) {
		
		Session session = null;

        try {

            session = sessionFactory.openSession();

            RegistrationLog registrationLog =
            		partnerRegistrationLogFactory.construct(request);

            try {

                session.beginTransaction();
                session.save(registrationLog);
                session.getTransaction().commit();

            } catch (HibernateException e) {

                session.getTransaction().rollback();
                throw e;

            }

            return
                    new PartnerSaleRegistrationIdImpl(
                    		registrationLog.getPartnerSaleRegistrationId());

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }
		

}
