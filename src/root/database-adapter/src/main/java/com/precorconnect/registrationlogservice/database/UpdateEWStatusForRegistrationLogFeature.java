package com.precorconnect.registrationlogservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;

public interface UpdateEWStatusForRegistrationLogFeature {
	
	void execute(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId
    );


}
