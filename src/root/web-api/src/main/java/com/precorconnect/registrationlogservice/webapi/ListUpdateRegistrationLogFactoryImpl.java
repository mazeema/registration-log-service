package com.precorconnect.registrationlogservice.webapi;

import org.springframework.stereotype.Component;

import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;
import com.precorconnect.registrationlogservice.SpiffStatus;
import com.precorconnect.registrationlogservice.SpiffStatusImpl;
import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLogImpl;

@Component
public class ListUpdateRegistrationLogFactoryImpl 
		implements ListUpdateRegistrationLogFactory {

	@Override
	public UpdatePartnerSaleRegistrationLog construct(
			UpdateRegistrationLog request) {
		PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(request.getPartnerSaleRegistrationId());
		
		SpiffStatus spiffStatus = new SpiffStatusImpl(request.getSpiffStatus());
		
		return new UpdatePartnerSaleRegistrationLogImpl(
				partnerSaleRegistrationId, 
				spiffStatus
		);
	}

}
