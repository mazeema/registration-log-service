package com.precorconnect.registrationlogservice.webapi;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.AccountName;
import com.precorconnect.AccountNameImpl;
import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLogImpl;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatus;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatusImpl;
import com.precorconnect.registrationlogservice.InstallDate;
import com.precorconnect.registrationlogservice.InstallDateImpl;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;
import com.precorconnect.registrationlogservice.SellDate;
import com.precorconnect.registrationlogservice.SellDateImpl;
import com.precorconnect.registrationlogservice.SpiffStatus;
import com.precorconnect.registrationlogservice.SpiffStatusImpl;
import com.precorconnect.registrationlogservice.SubmittedDate;
import com.precorconnect.registrationlogservice.SubmittedDateImpl;

@Component
public class AddRegistrationLogFactoryImpl 
			implements AddRegistrationLogFactory {

	@Override
	public AddPartnerSaleRegistrationLog construct(
			@NonNull AddRegistrationLog request
			) {
		
		PartnerSaleRegistrationId partnerSaleRegistrationId = new PartnerSaleRegistrationIdImpl(request.getPartnerSaleRegistrationId());
		
		AccountId accountId = new AccountIdImpl(request.getPartnerAccountId());
		
		AccountName accountName = new AccountNameImpl(request.getAccountName());
		
		try {
		
		SellDate sellDate = 
				new SellDateImpl(
					 new SimpleDateFormat("MM/dd/yyyy")
					 	.parse(request.getSellDate())
					 	.toInstant()
				);
		
		
		InstallDate installDate =
				request.getInstallDate().isPresent()?
				new InstallDateImpl(
						new SimpleDateFormat("MM/dd/yyyy")
						.parse(request.getInstallDate().get())
						.toInstant()
				)
				:null;
		
		SubmittedDate submittedDate = 
				new SubmittedDateImpl(
						new SimpleDateFormat("MM/dd/yyyy")
						.parse(request.getSubmittedDate())
						.toInstant()
				);
		
		ExtendedWarrantyStatus extendedWarrantyStatus = 
				new ExtendedWarrantyStatusImpl("Not Submitted");
		
		SpiffStatus spiffStatus = 
				new SpiffStatusImpl("Not Submitted");
		
		return 
				new AddPartnerSaleRegistrationLogImpl(
						partnerSaleRegistrationId, 
						accountId, 
						accountName, 
						sellDate, 
						installDate, 
						submittedDate, 
						extendedWarrantyStatus,
						spiffStatus
						);
	
		}catch (ParseException e) {
			e.printStackTrace();
		return null;
	}
 }

}
