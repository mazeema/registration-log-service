package com.precorconnect.registrationlogservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.ExtendedWarrantyStatusImpl;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationId;
import com.precorconnect.registrationlogservice.PartnerSaleRegistrationIdImpl;
import com.precorconnect.registrationlogservice.UpdatePartnerSaleRegistrationLog;
import com.precorconnect.registrationlogservice.api.RegistrationLogService;

@RestController
@RequestMapping("/registration-log")
@Api(value = "/registration-log", description = "Operations on registration log")
public class RegistrationLogResource {
	
	private final RegistrationLogService registrationLogService;
	
	private final OAuth2AccessTokenFactory auth2AccessTokenFactory;
	
	private final AddRegistrationLogFactory addRegistrationLogFactory;
	
	private final ListRegistrationLogFactory listRegistrationLogFactory;
	
	private final ListUpdateRegistrationLogFactory listUpdateRegistrationLogFactory;
	
	@Inject
	public RegistrationLogResource(
			@NonNull RegistrationLogService registrationLogService,
			@NonNull OAuth2AccessTokenFactory auth2AccessTokenFactory,
			@NonNull AddRegistrationLogFactory addRegistrationLogFactory,
			@NonNull ListRegistrationLogFactory listRegistrationLogFactory,
			@NonNull ListUpdateRegistrationLogFactory listUpdateRegistrationLogFactory
			) {
		
		this.registrationLogService =
                guardThat(
                        "registrationLogService",
                        registrationLogService
                )
                        .isNotNull()
                        .thenGetValue();
		
		
		this.auth2AccessTokenFactory =
                guardThat(
                        "auth2AccessTokenFactory",
                        auth2AccessTokenFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.addRegistrationLogFactory =
                guardThat(
                        "addRegistrationLogFactory",
                        addRegistrationLogFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.listRegistrationLogFactory =
                guardThat(
                        "listRegistrationLogFactory",
                        listRegistrationLogFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.listUpdateRegistrationLogFactory =
                guardThat(
                        "listUpdateRegistrationLogFactory",
                        listUpdateRegistrationLogFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
	}
	
	
	@RequestMapping(value = "/addRegistrationLog", method = RequestMethod.POST)
	@ApiOperation(value = "Add Registration Log")
	public Long addRegistrationLog(
			@RequestBody AddRegistrationLog request,
			@RequestHeader("Authorization") String authorizationHeader
			) throws AuthenticationException, AuthorizationException {
		
		OAuth2AccessToken oAuth2AccessToken = 
				auth2AccessTokenFactory
					.construct(authorizationHeader);
				
		
		PartnerSaleRegistrationId registrationLogId = 
				registrationLogService
					.addRegistrationLog(
						addRegistrationLogFactory.construct(request),
						oAuth2AccessToken
						);
							
		return 
				registrationLogId.getValue();
	}
	
	@RequestMapping(value = "/{accountId}",method = RequestMethod.GET)
    @ApiOperation(value = "get Registration Logs by accountId")
    public Collection<RegistrationLogSynopsisView> listRegistrationLogWithId(
    		@PathVariable("accountId") String accountId,
    		@RequestHeader("Authorization") String authorizationHeader
    		)throws AuthenticationException, AuthorizationException{

    	OAuth2AccessToken accessToken = 
    			auth2AccessTokenFactory
    				.construct(authorizationHeader);

    	return
    			registrationLogService
   						.listRegistrationLogWithId(
   								new AccountIdImpl(accountId),
   								accessToken)
   						.stream()
                        .map(listRegistrationLogFactory::construct)
                        .collect(Collectors.toList());

    }
	
	@RequestMapping(value = "/updateRegistrationLog",method = RequestMethod.PUT)
    @ApiOperation(value = "Update registration log using partnerSaleRegistrationId")
	public void updateRegistrationLog(
			@RequestBody List<UpdateRegistrationLog> request,
			@RequestHeader("Authorization") String authorizationHeader
			)throws AuthenticationException, AuthorizationException {
		
		OAuth2AccessToken accessToken = 
				auth2AccessTokenFactory
					.construct(authorizationHeader);
		
		List<UpdatePartnerSaleRegistrationLog> updatePartnerSaleRegistrationLogList = 
					request.stream()
				   .map(listUpdateRegistrationLogFactory::construct)
				   .collect(Collectors.toList());
		
		registrationLogService
					.updateRegistrationLog(
							updatePartnerSaleRegistrationLogList,		
							accessToken
				);
		
	}
	
	@RequestMapping(value = "/updateewstatusforregistrationlog",method = RequestMethod.POST)
    @ApiOperation(value = "Update EW Status For registration log using partnerSaleRegistrationId")
	public void updateEWStatusForRegistrationLog(
			@RequestParam("draftId") long draftId,
			@RequestParam("ewstatus") String ewStatus,
			@RequestHeader("Authorization") String authorizationHeader
			)throws AuthenticationException, AuthorizationException {
		
		OAuth2AccessToken accessToken = 
				auth2AccessTokenFactory
					.construct(authorizationHeader);
		
		
		
		registrationLogService
					.updateEWStatusForRegistrationLog(
							new ExtendedWarrantyStatusImpl(ewStatus), 
							new PartnerSaleRegistrationIdImpl(draftId),
							accessToken
					);
		
	}

}
