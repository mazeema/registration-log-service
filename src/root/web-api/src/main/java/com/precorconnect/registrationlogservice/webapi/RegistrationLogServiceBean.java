package com.precorconnect.registrationlogservice.webapi;

import javax.inject.Singleton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.precorconnect.registrationlogservice.api.RegistrationLogService;
import com.precorconnect.registrationlogservice.api.RegistrationLogServiceConfigFactoryImpl;
import com.precorconnect.registrationlogservice.api.RegistrationLogServiceImpl;

@Configuration
class RegistrationLogServiceBean {

	@Bean
	@Singleton
	public RegistrationLogService partnerSaleRegistrationService() {

		return 
				new RegistrationLogServiceImpl(
						new RegistrationLogServiceConfigFactoryImpl()
						.construct()
						);

	}

}
