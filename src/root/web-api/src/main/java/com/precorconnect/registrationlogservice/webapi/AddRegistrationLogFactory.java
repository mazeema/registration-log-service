package com.precorconnect.registrationlogservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.registrationlogservice.AddPartnerSaleRegistrationLog;

public interface AddRegistrationLogFactory {
	
	AddPartnerSaleRegistrationLog construct(
			@NonNull AddRegistrationLog addRegistrationLog
			);
             
   

}
