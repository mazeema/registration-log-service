package com.precorconnect.registrationlogservice.webapi;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features={"features/addRegistrationLog.feature"},
		glue={"com.precorconnect.registrationlogservice.webapi.addRegistrationLog"}
		)
public class AddRegistrationLogFeatureIT {
	
}
