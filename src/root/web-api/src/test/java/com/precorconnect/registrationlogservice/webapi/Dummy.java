package com.precorconnect.registrationlogservice.webapi;

import java.net.URI;
import java.net.URISyntaxException;

import com.precorconnect.OAuth2AccessToken;

public class Dummy {

    /*
    fields
     */
    private URI uri;

    private String accountId = "001A0000010mTyjIAE";
    
    private OAuth2AccessToken accessToken = null;
    
    private final Long partnerSaleRegistrationId = Long.valueOf(1);
    
    private final String accountName ="CucumberTest";
    
    private final String sellDate = "04/11/2016";

	private final String installDate = "05/02/2016";
	
	private final String submittedDate = "05/12/2016";
	
	private final String spiffStatus = "Not Submitted";
	
	private final String updateSpiffStatus = "Not Applicable";
    /*
    constructors
     */
    public Dummy() {

        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
        
    }

    /*
    getter methods
     */
    public URI getUri() {
        return uri;
    }

	public String getAccountId() {
		return accountId;
	}

	public OAuth2AccessToken getAccessToken() {
		return accessToken;
	}

	public String getAccountName() {
		return accountName;
	}

	public String getSellDate() {
		return sellDate;
	}

	public String getInstallDate() {
		return installDate;
	}

	public String getSubmittedDate() {
		return submittedDate;
	}

	public String getSpiffStatus() {
		return spiffStatus;
	}

	public String getUpdateSpiffStatus() {
		return updateSpiffStatus;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

}
