package com.precorconnect.registrationlogservice.webapi;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class RegistrationLogResourceIT {

    
   
     

    private final Dummy dummy =
    		 new Dummy();

    private final Config config =
            new ConfigFactory()
                    .construct();


    private final Factory factory =
            new Factory(
            		dummy,
            		new IdentityServiceIntegrationTestSdkImpl(
                    		config.getIdentityServiceJwtSigningKey()
                    )
            );


    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {

        RestAssured.port = port;
    }

   
    @Test
    public void addRegistrationLog_ReturnsRegistrationId(
    )  {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();
    	
    	AddRegistrationLog addRegistrationLog = new AddRegistrationLog(
				dummy.getPartnerSaleRegistrationId(),
				dummy.getAccountId(),
				dummy.getAccountName(),
				dummy.getSellDate(),
				dummy.getInstallDate(),
				dummy.getSubmittedDate()
				);
    	
    	 given()
         .header(
                 "Authorization",
                 authToken
         )
 		.header(
 				"Content-Type",
 				"application/json"
 				)
         .body(addRegistrationLog)
         .post("/registration-log/addRegistrationLog")
         .then()
         .assertThat()
         .statusCode(200);

    }

    

    @Test
    public void listRegistrationLogWithPartnerId_ReturnsListofRegistrations() {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();

        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                authToken
                        )
                )
                .get("/registration-log/"+dummy.getAccountId()
                )
                .then()
                .assertThat()
                .statusCode(200);

    }


    @Test
    public void updateRegistrationLog_ReturnsNothing() {

    	String authToken = factory
    			.constructValidPartnerRepOAuth2AccessToken()
    			.getValue();
    	
    	List<UpdateRegistrationLog> updateRegistrationLogs = new ArrayList<UpdateRegistrationLog>();
    	
    	UpdateRegistrationLog updateRegistrationLog = new UpdateRegistrationLog(
				dummy.getPartnerSaleRegistrationId(),
				dummy.getUpdateSpiffStatus()
				);
    	updateRegistrationLogs.add(updateRegistrationLog);
        given()
                .header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                authToken
                        )
                )
                .contentType(ContentType.JSON)
                .body(updateRegistrationLogs)
                .put("/registration-log/updateRegistrationLog")
                .then()
                .assertThat()
                .statusCode(200);

    }

}