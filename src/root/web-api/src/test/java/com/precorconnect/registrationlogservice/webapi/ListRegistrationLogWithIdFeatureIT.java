package com.precorconnect.registrationlogservice.webapi;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features={"features/listRegistrationLog.feature"},
		glue={"com.precorconnect.registrationlogservice.webapi.listRegistrationLog"}
		)
public class ListRegistrationLogWithIdFeatureIT {

}
