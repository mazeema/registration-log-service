package com.precorconnect.registrationlogservice.webapi.addRegistrationLog;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Value;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.registrationlogservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;
import com.precorconnect.registrationlogservice.webapi.Config;
import com.precorconnect.registrationlogservice.webapi.ConfigFactory;
import com.precorconnect.registrationlogservice.webapi.Dummy;
import com.precorconnect.registrationlogservice.webapi.Factory;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs extends AbstractSpringIntegrationTest {
	
	
	
	 private final Config config =
	            new ConfigFactory().construct();

	    private final Dummy dummy = new Dummy();

	    private final Factory factory =
	            new Factory(
	                    dummy,
	                    new IdentityServiceIntegrationTestSdkImpl(
	                            config.getIdentityServiceJwtSigningKey()
	                    )
	            );

	    private OAuth2AccessToken accessToken;
	    
	    private AddRegistrationLog addRegistrationLog;

	    private Response registrationId;
	    
	    @Before
	    public void beforeAll() {

	        RestAssured.port = getPort();

	    }
	    

		@Given("^an addRegistrationLog consists of:$")
		public void an_addRegistrationLog_consists_of(DataTable arg1) throws Throwable {
		    // No op add registration log dummy
		}

		@Given("^I provide an accessToken identifying me as <identity>$")
		public void i_provide_an_accessToken_identifying_me_as_identity() throws Throwable {
			
			 accessToken = new OAuth2AccessTokenImpl(
						factory
							.constructValidPartnerRepOAuth2AccessToken()
							.getValue()
						);
		}

		@Given("^provide a valid addRegistrationLog$")
		public void provide_a_valid_addRegistrationLog() throws Throwable {
			addRegistrationLog = new AddRegistrationLog(
					dummy.getPartnerSaleRegistrationId(),
					dummy.getAccountId(),
					dummy.getAccountName(),
					dummy.getSellDate(),
					dummy.getInstallDate(),
					dummy.getSubmittedDate()
					);
		}

		@When("^I POST to /addRegistrationLog$")
		public void i_POST_to_addRegistrationLog() throws Throwable {
			registrationId =
	    			given()
	                .contentType(ContentType.JSON)
	                .header(
	                        "Authorization",
	                        String.format(
	                                "Bearer %s",
	                                accessToken
	                                        .getValue()
	                        )
	                )
	                .body(addRegistrationLog)
	                .post("/registration-log/addRegistrationLog");
		}

		@Then("^a registration log is added to the registration-log table with the provided attributes$")
		public void a_registration_log_is_added_to_the_registration_log_table_with_the_provided_attributes() throws Throwable {

			registrationId
										.then()
										.assertThat()
										.statusCode(200);
		}

		@Then("^its id is returned$")
		public void its_id_is_returned() throws Throwable {
		   Assert.assertNotNull(registrationId);
		}


}
