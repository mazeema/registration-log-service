package com.precorconnect.registrationlogservice.webapi.updateRegistrationLog;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.OAuth2AccessTokenImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.registrationlogservice.webapi.AbstractSpringIntegrationTest;
import com.precorconnect.registrationlogservice.webapi.Config;
import com.precorconnect.registrationlogservice.webapi.ConfigFactory;
import com.precorconnect.registrationlogservice.webapi.Dummy;
import com.precorconnect.registrationlogservice.webapi.Factory;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefs 
			extends AbstractSpringIntegrationTest {
		
		 private final Config config =
		            new ConfigFactory().construct();

		    private final Dummy dummy = new Dummy();

		    private final Factory factory =
		            new Factory(
		                    dummy,
		                    new IdentityServiceIntegrationTestSdkImpl(
		                            config.getIdentityServiceJwtSigningKey()
		                    )
		            );

		    private OAuth2AccessToken accessToken;
		    
		    
		    List<UpdateRegistrationLog> updateRegistrationLogs = new ArrayList<UpdateRegistrationLog>();
		    
		    private Response response;

		    
		    @Before
		    public void beforeAll() {

		        RestAssured.port = getPort();

		    }
		    
		    @Given("^an updateRegistrationLog consists of:$")
		    public void an_updateRegistrationLog_consists_of(DataTable arg1) throws Throwable {
		    	// No op update registration log from dummy
		    }

		    @Given("^I provide an accessToken identifying me as <identity>$")
		    public void i_provide_an_accessToken_identifying_me_as_identity() throws Throwable {
		    	
		    	accessToken = new OAuth2AccessTokenImpl(
						factory
							.constructValidPartnerRepOAuth2AccessToken()
							.getValue()
						);
		    }

		    @Given("^provide a valid updateRegistrationLog$")
		    public void provide_a_valid_updateRegistrationLog() throws Throwable {
		    	
		    	UpdateRegistrationLog updateRegistrationLog = new UpdateRegistrationLog(
						dummy.getPartnerSaleRegistrationId(),
						dummy.getSpiffStatus()
						);
		    	
		    	updateRegistrationLogs.add(updateRegistrationLog);
		    }

		    @When("^I PUT to /updateRegistrationLog$")
		    public void i_PUT_to_updateRegistrationLog() throws Throwable {
		    	
		    	response =
		    			given()
		                .contentType(ContentType.JSON)
		                .header(
		                        "Authorization",
		                        String.format(
		                                "Bearer %s",
		                                accessToken
		                                        .getValue()
		                        )
		                )
		                .body(updateRegistrationLogs)
		                .put("/registration-log/updateRegistrationLog");
		    }

		    @Then("^a registration log is updates the registration-log table with spiff status for given partner sale registration id$")
		    public void a_registration_log_is_updates_the_registration_log_table_with_spiff_status_for_given_partner_sale_registration_id() throws Throwable {
		    	
		    	response
		    			.then()
		    			.assertThat()
		    			.statusCode(200);
		    }


}
