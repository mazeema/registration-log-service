## Description
Precor Connect Registration Log service API for ReST.

## Features

##### Add Registration Log
* [documentation](features/updateRegistrationLog.feature)

##### List Registration Log
* [documentation](features/updateRegistrationLog.feature)

##### Update Registration Log Status
* [documentation](features/updateRegistrationLog.feature)


## API Explorer

##### Environments:
-  [dev](https://registration-log-service-dev.precorconnect.com/)
-  [qa](https://registration-log-service-qa.precorconnect.com/)
-  [prod](https://registration-log-service-prod.precorconnect.com/)