package com.precorconnect.registrationlogservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class RegistrationLogSynopsisView {
	
	/*
	 * fields
	 */
	private final Long registrationId;
	private final Long partnerSaleRegistrationId;
	private final String accountId;
	private final String sellDate;
	private final String installDate;
	private final String submittedDate;
	private final String facilityName;
	private final String extendedWarrantyStatus;
	private final String spiffStatus;
	
	public RegistrationLogSynopsisView(
			@NonNull Long registrationId,
			@NonNull Long partnerSaleRegistrationId,
			@NonNull String accountId,
			@NonNull String sellDate,
			@Nullable String installDate,
			@NonNull String submittedDate,
			@NonNull String facilityName,
			@NonNull String extendedWarrantyStatus,
			@NonNull String spiffStatus
			) {

		 
		this.registrationId =
                guardThat(
                        "registrationId",
                        registrationId
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.accountId =
                guardThat(
                        "accountId",
                        accountId
                )
                        .isNotNull()
                        .thenGetValue();

		this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate = installDate;

		this.submittedDate =
                guardThat(
                        "submittedDate",
                        submittedDate
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.facilityName =
                guardThat(
                        "facilityName",
                        facilityName
                )
                        .isNotNull()
                        .thenGetValue();
		
		
		this.extendedWarrantyStatus =
                guardThat(
                        "extendedWarrantyStatus",
                        extendedWarrantyStatus
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffStatus =
                guardThat(
                        "spiffStatus",
                        spiffStatus
                )
                        .isNotNull()
                        .thenGetValue();

	}

	public Long getRegistrationId() {
		return registrationId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getAccountId() {
		return accountId;
	}

	public String getSellDate() {
		return sellDate;
	}

	public Optional<String> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	public String getSubmittedDate() {
		return submittedDate;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getExtendedWarrantyStatus() {
		return extendedWarrantyStatus;
	}

	public String getSpiffStatus() {
		return spiffStatus;
	}
	
}
