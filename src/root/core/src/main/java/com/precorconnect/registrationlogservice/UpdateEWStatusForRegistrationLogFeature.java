package com.precorconnect.registrationlogservice;


import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateEWStatusForRegistrationLogFeature {
	
	void execute(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId
    );

}
