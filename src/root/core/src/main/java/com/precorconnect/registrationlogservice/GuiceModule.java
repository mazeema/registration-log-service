package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;

class GuiceModule extends
        AbstractModule {

    private final DatabaseAdapter databaseAdapter;

    private final IdentityServiceAdapter identityServiceAdapter;

    public GuiceModule(
            @NonNull final DatabaseAdapter databaseAdapter,
            @NonNull final IdentityServiceAdapter identityServiceAdapter
            ){
    	
    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.identityServiceAdapter =
                guardThat(
                        "identityServiceAdapter",
                         identityServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindAdapters();
        bindFeatures();

    }

    private void bindAdapters() {

        bind(DatabaseAdapter.class)
                .toInstance(databaseAdapter);

        bind(IdentityServiceAdapter.class)
        		.toInstance(identityServiceAdapter);

    }

    private void bindFeatures() {
    	
    	 bind(AddRegistrationLogFeature.class)
 				.to(AddRegistrationLogFeatureImpl.class);

        bind(ListRegistrationLogFeature.class)
        		.to(ListRegistrationLogFeatureImpl.class);
        
        bind(UpdateRegistrationLogFeature.class)
			.to(UpdateRegistrationLogFeatureImpl.class);
        
        bind(UpdateEWStatusForRegistrationLogFeature.class)
		.to(UpdateEWStatusForRegistrationLogFeatureImpl.class);

    }
}
