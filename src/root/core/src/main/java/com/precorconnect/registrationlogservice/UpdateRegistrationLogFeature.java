package com.precorconnect.registrationlogservice;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateRegistrationLogFeature {
	
	void execute(
            @NonNull List<UpdatePartnerSaleRegistrationLog> request
    );

}
