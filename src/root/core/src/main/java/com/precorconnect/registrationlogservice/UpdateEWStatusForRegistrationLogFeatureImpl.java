package com.precorconnect.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

@Singleton
public class UpdateEWStatusForRegistrationLogFeatureImpl implements UpdateEWStatusForRegistrationLogFeature {
	
	 /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public UpdateEWStatusForRegistrationLogFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }
    

	@Override
	public void execute(@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId) {
		
		
		databaseAdapter
					.updateEWStatusForRegistrationLog(extendedWarrantyStatus, partnerSaleRegistrationId);

	}

}
