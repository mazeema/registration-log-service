package com.precorconnect.registrationlogservice;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;


public class CoreImpl 
				implements Core {
        

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public CoreImpl(
            @NonNull DatabaseAdapter databaseAdapter,
            @NonNull IdentityServiceAdapter identityServiceAdapter
    ) {

    	GuiceModule guiceModule =
                new GuiceModule(
                        databaseAdapter,
                        identityServiceAdapter
                        );


        injector = Guice.createInjector(guiceModule);
    }

  
	@Override
	public Collection<RegistrationLogView> listRegistrationLogWithId(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {

		injector
			.getInstance(IdentityServiceAdapter.class)
			.getAccessContext(accessToken);

		return
				injector
					.getInstance(ListRegistrationLogFeature.class)
					.listRegistrationLogWithId(accountId);

	}


	@Override
	public PartnerSaleRegistrationId addRegistrationLog(
			@NonNull AddPartnerSaleRegistrationLog request,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException{
		
		injector
			.getInstance(IdentityServiceAdapter.class)
			.getAccessContext(accessToken);

	return
			injector
			.getInstance(AddRegistrationLogFeature.class)
			.execute(request);
	}


	@Override
	public void updateRegistrationLog(
			@NonNull List<UpdatePartnerSaleRegistrationLog> request, 
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {
		
		injector
			.getInstance(IdentityServiceAdapter.class)
			.getAccessContext(accessToken);
		
		injector
			.getInstance(UpdateRegistrationLogFeature.class)
			.execute(request);
		
	}


	@Override
	public void updateEWStatusForRegistrationLog(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId, 
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException {

		injector
		.getInstance(IdentityServiceAdapter.class)
		.getAccessContext(accessToken);
		
		injector
		      .getInstance(UpdateEWStatusForRegistrationLogFeature.class)
		      .execute(extendedWarrantyStatus, partnerSaleRegistrationId);
		
	}


}
