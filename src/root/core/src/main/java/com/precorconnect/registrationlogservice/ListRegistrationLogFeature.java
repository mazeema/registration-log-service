package com.precorconnect.registrationlogservice;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;

public interface ListRegistrationLogFeature {

	Collection<RegistrationLogView> listRegistrationLogWithId(
			@NonNull AccountId accountId
			) throws AuthenticationException ;
	
}
