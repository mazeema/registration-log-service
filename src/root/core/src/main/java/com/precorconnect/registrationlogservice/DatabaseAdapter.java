package com.precorconnect.registrationlogservice;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;


public interface DatabaseAdapter {
	
	PartnerSaleRegistrationId 
			addRegistrationLog(
					@NonNull AddPartnerSaleRegistrationLog request
					);

    Collection<RegistrationLogView> 
    		listSpiffLogsWithId(
    				@NonNull AccountId accountId
    				);
    
    void updateRegistrationLog(
					@NonNull List<UpdatePartnerSaleRegistrationLog> request
					);
    
    void updateEWStatusForRegistrationLog(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId			
			);

}
