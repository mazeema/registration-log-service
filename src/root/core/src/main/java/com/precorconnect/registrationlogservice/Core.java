package com.precorconnect.registrationlogservice;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;

public interface Core {
	
	PartnerSaleRegistrationId 
			addRegistrationLog( 
					@NonNull AddPartnerSaleRegistrationLog request,
					@NonNull OAuth2AccessToken accessToken
					) throws AuthenticationException, AuthorizationException;

	Collection<RegistrationLogView> 
				listRegistrationLogWithId(
					@NonNull AccountId accountId,
					@NonNull OAuth2AccessToken accessToken
					) throws AuthenticationException, AuthorizationException;
	

	void updateRegistrationLog(
					@NonNull List<UpdatePartnerSaleRegistrationLog> request,
					@NonNull OAuth2AccessToken accessToken
					) throws AuthenticationException, AuthorizationException;
	
	void updateEWStatusForRegistrationLog(
			@NonNull ExtendedWarrantyStatus extendedWarrantyStatus,
			@NonNull PartnerSaleRegistrationId partnerSaleRegistrationId,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException;

}
